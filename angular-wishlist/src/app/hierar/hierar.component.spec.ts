import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HierarComponent } from './hierar.component';

describe('HierarComponent', () => {
  let component: HierarComponent;
  let fixture: ComponentFixture<HierarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HierarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HierarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
